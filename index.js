// alert("HI");

let string1 = "Zuitt";
let string2 = "Coding";
let string3 = "Bootcamp";
let string4 = "teaches";
let string5 = "javascript";


// ES6 Updates
	//EcmaScript6 or ES6 is the new version of javascript
	//js is formally know as EcmaScript.
	//ECMA - European Computer Manufacturers Association Script
		// ECMAScript standard defines the rules, details, and guidelines that the scripting language must observe to be considered ECMAScript complaint.
//Template Liretals are part of ES6 Updates
	// `` - backticks
	// ${}-placeholder


let sentenceString = (`${string1} ${string2} ${string3} ${string4} ${string5}`);
console.log(sentenceString);

	//Concatenating strings with +
let pharse = "Hello" + " " + "World"
console.log(pharse);

// Exponent Operator (**) - ES6 updates
	let fivePowerOf2 = 5 ** 2
	console.log(fivePowerOf2);
	//result: 25

// Using Math.pow(base, exponent)
	let fivePowerOf3 = 5 ** 3
	console.log(fivePowerOf3);
		//result: 125

// Template Literals with JS Expression
	let sentence2 = `The result of five to the power of 2 is ${5**2}`
	console.log(sentence2);

// Array Destructuring - this will allow us to save array items in a variable

	let array = ["Kobe", "Lebron", "Shaq", "Westbook"];
	console.log(array[2])
		//result: Shaq

	let lakerPlayer1 = array[3];
		//Westbook
	let lakerPlayer2 = array[0];
		//Kobe

//Array Destructuting
//use const when you want to assign a variable  intead of [indexes]
//dont use strings or numbers
//can use empty spaces
	const [kobe, lebron, , wesbrook] = array;

	console.log(wesbrook);
	//Westbook
	// console.log(lebron);
	// 	//Lebron
	// console.log(shaq);
	// 	//Shaq

//MINI ACTIVITY

let array2 = ["Curry", "Lillard", "Paul", "Irving"];
const [pointGuard1, pointGuard2, pointGuard3, pointGuard4] = array2;
console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);

let bandMembers = ["Hayley", "Zac", "Jeremy", "Taylor"];
const [vocal, lead, , bass] = bandMembers;
console.log(vocal);
console.log(lead);
console.log(bass);
//note: order matters in an array destructuring 
	//you can skip an item by adding another separtor (,) but no variabe name

//Object Destructuring
	//this will allow us to destructure an objec by allowing us to add the values of an object's property into respective variables.

let person = {
	name : "Jeremy Davis",
	birtday : "September 12, 1989",
	age : 32
};

const {age, firstName, birtday} = person;
console.log(age);
console.log(firstName);
	//result: undefined since person.firstName is not a property of a person
console.log(birtday);
//order doest not matter in object destructuring

let pokemon1 = {
	name: "Charmander",
	level: 11,
	type: "Fire",
	moves: ["Ember", "Scratch", "Leer"]
};

//MINI ACTIVITY

const {name, level, type, moves } = pokemon1;

let sentencePokemon = `My pokemon is ${name}, it is in level ${level}. It is a ${type}. It's moves are ${moves}.`
console.log(sentencePokemon);

// Arrow Functions
		// are alternative way of writing functions. Howeverm arrow functions have sinigticant pros and cons against the use of tradition function.

//traditional function
function displayMsg(){
	console.log("Hello World!");
}

// arrow function
// => FAT ARROW
const hello = () => {
	console.log("Hello World, Again!");
}

displayMsg();
hello();

const greet = (person) => {
	console.log(`Hi ${person.name}`)
};

greet(person);

// Implicit Return
	//allow us to return a value with the use of return keyword
	//should be on line
const addNum = (num1, num2) => num1 + num2
let sum = addNum(2,4);
console.log(sum);
// let band = bandMembers.forEach((person) => {statement})

//THIS KEYWORD

let protaganist = {
	name: "Cloud Strife",
	occupation: "SOLDIER",
		// traditional method would have this keyword refer to the parent object
	greet: function (){
		console.log(this);
		console.log(`Hi I am ${this.name}`);
	},
	// arrow function refer to the global window
	introduce: () => {
		console.log(this);
		console.log(`I work as ${this.occupation}`);
	}
};

protaganist.greet();
protaganist.introduce();

//Class-Base Objects Blueprints
	//Classes are templates of object.
	//We can create objects our of the use of classes.

function Pokemon(name, type, lvl){
	this.name = name;
	this.type = type;
	this.lvl = lvl;
};

// ES6 Class Creation

class Car {
	//CONSTRUCTOR - holds the properties.
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
};

let car1 = new Car ("Toyota", "Vios", "2002");
console.log(car1);
let car2 = new Car("Cooper", "Mini", "1969");
console.log(car2);