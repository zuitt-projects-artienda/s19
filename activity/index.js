/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
*/

let student1 = {
	//adde comma
	name: "Shawn Michaels",
	// added the missing pair of double quote
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	// added comma
	classes: ["Philosphy 101", "Social Sciences 201"]
	//added ;
};

let student2 = {
	//added :
	name: "Steve Austin",
	birthday: "June 15, 2001",
	//added comma
	age: 20,
	//added comma
	isEnrolled: true,
	//added pair of square bracket and removed comma
	classes: ["Philosphy 401", "Natural Sciences 402"]
};

//turned to arrow function
const introduce = (student) => {
	console.log(`Hi! I'm ${student.name}. I am ${student.age} years old.`);
	console.log(`I study the following courses: ${student.classes}`);
};

let studentIntro1 = introduce(student1);

//turned to arrow function
const getCube = (num) => Math.pow(num,3);

let cube = getCube(3);

console.log(cube)


let numArr = [15,16,32,21,21,2];
numArr.forEach(numArray => console.log(numArray));


const squaredFunction = (num) => num ** 2;
let numSquared = numArr.map(squaredFunction);
console.log(numSquared);


/*
	2. Create a class constructor able to receive 3 arguments
		-It should be able to receive two strings and a number
		-Using the this keyword assign properties:
			name, 
			breed, 
			dogAge = <7x human years> 
			-assign the parameters as values to each property.

	Create 2 new objects using our class constructor.

	This constructor should be able to create Dog objects.

	Log the 2 new Dog objects in the console.

*/


class Dog {
	constructor(name, breed, dogAge){
		this.name = name;
		this.breed = breed;
		this.dogAge = dogAge * 7;
	}
};

let dog1 = new Dog ("Pampu", "Askal", 3);
console.log(dog1);
let dog2 = new Dog ("Jager", "Shih Tzu", 2);
console.log(dog2);
